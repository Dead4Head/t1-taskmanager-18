package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user) throws AbstractException;

    List<User> findAll();

    User findOneById(String id) throws AbstractException;

    User findOneByLogin(String login) throws AbstractException;

    User findOneByEmail(String email) throws AbstractException;

    User remove(User user) throws AbstractException;

    Boolean isLoginExist(String login) throws AbstractException;

    Boolean isEmailExist(String email) throws AbstractException;

}

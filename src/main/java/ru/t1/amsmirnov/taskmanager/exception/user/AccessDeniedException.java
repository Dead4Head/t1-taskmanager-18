package ru.t1.amsmirnov.taskmanager.exception.user;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access to this action denied by AuthService...");
    }

}

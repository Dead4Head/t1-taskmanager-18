package ru.t1.amsmirnov.taskmanager.service;

import ru.t1.amsmirnov.taskmanager.api.service.ILoggerService;

import java.io.InputStream;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private static final String CONFIG_FILE = "/logger.properties";
    private static final String COMMANDS = "COMMANDS";
    private static final String COMMAND_FILE = "./commands.xml";
    private static final String ERRORS = "ERRORS";
    private static final String ERROR_FILE = "./errors.xml";
    private static final String MESSAGES = "MESSAGES";
    private static final String MESSAGE_FILE = "./messages.xml";

    private static final LogManager MANAGER = LogManager.getLogManager();
    private static final Logger LOGGER_ROOT = Logger.getLogger("");
    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);
    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);
    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    static {
        loadConfigFromFile();
        registry(LOGGER_COMMAND, COMMAND_FILE, false);
        registry(LOGGER_ERROR, ERROR_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGE_FILE, true);
    }

    private static void loadConfigFromFile() {
        try {
            final Class<?> clazz = LoggerService.class;
            final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(inputStream);
        } catch (Exception exception) {
            LOGGER_ROOT.severe(exception.getMessage());
        }
    }

    private static void registry(
            final Logger logger,
            final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty())
                logger.addHandler(new FileHandler(fileName));
        } catch (Exception exception) {
            LOGGER_ROOT.severe(exception.getMessage());
        }
    }

    private static ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    public static Logger getLoggerCommand() {
        return LOGGER_COMMAND;
    }

    public static Logger getLoggerError() {
        return LOGGER_ERROR;
    }

    public static Logger getLoggerMessage() {
        return LOGGER_MESSAGE;
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void error(final Exception exception) {
        if (exception == null) return;
        LOGGER_ERROR.log(Level.SEVERE, exception.getMessage(), exception);
    }

}
